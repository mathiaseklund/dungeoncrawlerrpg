package me.mathiaseklund.dungeoncrawler;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.dungeoncrawler.commands.CommandSetSpawn;
import me.mathiaseklund.dungeoncrawler.commands.CommandSpawn;
import me.mathiaseklund.dungeoncrawler.commands.character.CommandCharacter;
import me.mathiaseklund.dungeoncrawler.users.UserListener;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class Main extends JavaPlugin {

	static Main main;

	// Files
	File messagesFile;
	FileConfiguration messages;

	/**
	 * Get instance of Main class
	 * 
	 * @return Main class
	 */
	public static Main getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();
		loadWorlds();

		LoginOnlinePlayers();
	}

	void loadWorlds() {
		getServer().createWorld(new WorldCreator("act1"));
		getServer().createWorld(new WorldCreator("selectchar"));
	}

	/**
	 * Loa default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save messages file
	 */
	public void saveMessages() {
		try {
			messages.save(messagesFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Messages config
	 * 
	 * @return
	 */
	public FileConfiguration getMessages() {
		return this.messages;
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
		getServer().getPluginManager().registerEvents(new UserListener(), this);
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {
		getCommand("setspawn").setExecutor(new CommandSetSpawn());
		getCommand("spawn").setExecutor(new CommandSpawn());
		getCommand("character").setExecutor(new CommandCharacter());
	}

	/**
	 * Relogin all online players.
	 */
	void LoginOnlinePlayers() {
		Util.debug("Relogging in all online players.");
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.closeInventory();
			getServer().getPluginManager().callEvent(new PlayerJoinEvent(p, null));
		}
	}
}
