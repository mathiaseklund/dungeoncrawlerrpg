package me.mathiaseklund.dungeoncrawler.characters;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.users.User;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class CharacterAPI {

	public static void createCharacter(User user, String name, CharacterClass charClass, boolean hardcore) {
		if (!characterExists(name)) {
			Util.debug("Creating new character with params: " + name + " | " + charClass.toString() + " | " + hardcore);
			new Character(user, name, charClass, hardcore);
		} else {
			Player p = user.getPlayer();
			if (p != null) {
				Util.message(p, "A Character already exists with requested name");
			}
		}
	}

	/**
	 * Check if a character by specified name exists.
	 * 
	 * @param name
	 * @return
	 */
	public static boolean characterExists(String name) {
		Main main = Main.getMain();
		File f = new File(main.getDataFolder() + "/characters/" + name + ".yml");
		if (f.exists() && !f.isDirectory()) {
			// File exists.
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if uuid is the owner of character
	 * 
	 * @param uuid
	 * @param name
	 * @return
	 */
	public static boolean isOwner(String uuid, String name) {
		Main main = Main.getMain();
		File f = new File(main.getDataFolder() + "/characters/" + name + ".yml");
		FileConfiguration data = YamlConfiguration.loadConfiguration(f);
		String owner = data.getString("owner");
		if (owner.equalsIgnoreCase(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Select character
	 * 
	 * @param user
	 * @param name
	 */
	public static void selectCharacter(User user, String name) {
		new Character(user, name);
	}
}
