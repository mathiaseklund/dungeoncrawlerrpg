package me.mathiaseklund.dungeoncrawler.characters;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.users.User;

public class Character {

	File file;
	FileConfiguration data;
	Main main = Main.getMain();

	User user;
	String name;
	CharacterClass charClass;
	boolean hardcore;
	double experience;

	public Character(User user, String name, CharacterClass charClass, boolean hardcore) {
		// Creates a new character
		this.user = user;
		this.name = name;
		this.charClass = charClass;
		this.hardcore = hardcore;
		create();
	}

	public Character(User user, String name) {
		// Selects a character.
		this.user = user;
		this.name = name;
		load();

		user.selectCharacter(this);
	}

	void create() {
		load();
		data.set("owner", user.getUUID());
		data.set("name", name);
		data.set("class", charClass.toString());
		data.set("hardcore", hardcore);
		data.set("experience", 0);
		save();
		user.selectCharacter(this);
	}

	void load() {
		file = new File(main.getDataFolder() + "/characters/" + name + ".yml");
		data = YamlConfiguration.loadConfiguration(file);
		save();
	}

	void save() {
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
