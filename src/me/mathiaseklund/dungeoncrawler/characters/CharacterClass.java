package me.mathiaseklund.dungeoncrawler.characters;

public enum CharacterClass {
	AMAZON, ASSASSIN, BARBARIAN, DRUID, NECROMANCER, PALADIN, SORCERESS
}
