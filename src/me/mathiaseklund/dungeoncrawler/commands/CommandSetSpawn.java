package me.mathiaseklund.dungeoncrawler.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.users.Users;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class CommandSetSpawn implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			String uuid = ((Player) sender).getUniqueId().toString();
			if (Users.isStaff(uuid)) {
				if (args.length == 0) {
					Util.message(sender, "Command must have arguments. Example: /setspawn lobby");
				} else if (args.length > 0) {
					String spawnType = args[0];
					if (spawnType.equalsIgnoreCase("lobby")) {
						// Set spawn for the character lobby area.
						Location loc = ((Player) sender).getLocation();
						String location = Util.LocationToString(loc);
						main.getConfig().set("spawn." + spawnType, location);
						main.saveConfig();

						Util.message(sender, "New " + spawnType + " spawn location set.");
					} else if (spawnType.equalsIgnoreCase("act1")) {
						// Set spawn for the character act1 spawn area.
						Location loc = ((Player) sender).getLocation();
						String location = Util.LocationToString(loc);
						main.getConfig().set("spawn." + spawnType, location);
						main.saveConfig();

						Util.message(sender, "New " + spawnType + " spawn location set.");
					} else {
						Util.message(sender, "Spawn Type invalid.");
					}
				}
			} else {
				Util.message(sender, "Command can only be run by Staff members.");
			}
		} else {
			Util.message(sender, "Command is not available for console users.");
		}
		return false;
	}

}
