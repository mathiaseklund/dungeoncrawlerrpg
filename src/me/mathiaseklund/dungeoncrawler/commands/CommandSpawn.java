package me.mathiaseklund.dungeoncrawler.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class CommandSpawn implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			if (args.length == 0) {
				Util.message(sender, "Please specify spawntype: lobby, act1");
			} else if (args.length > 0) {
				String spawnType = args[0].toLowerCase();
				String locString = main.getConfig().getString("spawn." + spawnType);
				if (locString != null) {
					Location loc = Util.StringToLocation(locString);
					((Player) sender).teleport(loc);
				} else {
					Util.message(sender, "SpawnType does not exist.");
				}
			}
		} else {
			Util.message(sender, "Command can only be run by a player.");
		}
		return false;
	}

}
