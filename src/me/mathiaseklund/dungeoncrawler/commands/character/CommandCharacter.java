package me.mathiaseklund.dungeoncrawler.commands.character;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.characters.CharacterAPI;
import me.mathiaseklund.dungeoncrawler.characters.CharacterClass;
import me.mathiaseklund.dungeoncrawler.users.User;
import me.mathiaseklund.dungeoncrawler.users.Users;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class CommandCharacter implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			String uuid = player.getUniqueId().toString();
			User user = Users.getUser(uuid);
			if (args.length > 1) {
				if (args[0].equalsIgnoreCase("create")) {
					if (args.length <= 2) {
						Util.message(sender, "### USAGE");
						Util.message(sender, "/ch create <name> <class> [hardcore(y/n)]");
					} else if (args.length > 2) {
						String name = args[1];
						String characterClass = args[2].toUpperCase();
						boolean hardcore = false;
						if (args.length == 4) {
							if (args[3].equalsIgnoreCase("y")) {
								hardcore = true;
							}
						}
						CharacterAPI.createCharacter(user, name, CharacterClass.valueOf(characterClass), hardcore);
					}
				} else if (args[0].equalsIgnoreCase("select")) {
					String name = args[1];
					if (CharacterAPI.characterExists(name)) {
						if (CharacterAPI.isOwner(user.getUUID(), name)) {
							CharacterAPI.selectCharacter(user, name);
						}
					}
				}
			} else {
				Util.message(sender, "### USAGE");
				Util.message(sender, "/ch create <name> <class> [hardcore(y/n)]");
				Util.message(sender, "/ch select <name>");
			}
		} else {
			Util.message(sender, "Command can only be run by a player.");
		}
		return false;
	}

}
