package me.mathiaseklund.dungeoncrawler.users;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class User {

	String uuid;
	File file;
	FileConfiguration data;
	Main main = Main.getMain();

	UserRole role;
	me.mathiaseklund.dungeoncrawler.characters.Character selectedCharacter;

	public User(String uuid) {
		this.uuid = uuid;
		load();
	}

	/**
	 * Load user data
	 */
	void load() {
		file = new File(main.getDataFolder() + "/users/" + uuid + ".yml");
		data = YamlConfiguration.loadConfiguration(file);
		save();
	}

	/**
	 * Save user data
	 */
	void save() {
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get user UUID
	 * 
	 * @return
	 */
	public String getUUID() {
		return this.uuid;
	}

	/**
	 * Get user role
	 * 
	 * @return
	 */
	public UserRole getRole() {
		if (role != null) {
			return role;
		} else {
			if (data.getString("role") != null) {
				UserRole role = UserRole.valueOf(data.getString("role"));
				setRole(role);
				return role;
			} else {
				setRole(UserRole.PLAYER);
				return UserRole.PLAYER;
			}
		}
	}

	/**
	 * Set user role
	 * 
	 * @param role
	 */
	public void setRole(UserRole role) {
		this.role = role;
		data.set("role", role.toString());
		save();
	}

	/**
	 * Get user player object
	 * 
	 * @return
	 */
	public Player getPlayer() {
		Player p = Bukkit.getPlayer(UUID.fromString(uuid));
		return p;
	}

	/**
	 * Get user offline player object
	 * 
	 * @return
	 */
	public OfflinePlayer getOfflinePlayer() {
		OfflinePlayer op = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
		return op;
	}

	public void selectCharacter(me.mathiaseklund.dungeoncrawler.characters.Character character) {
		Main main = Main.getMain();
		this.selectedCharacter = character;
		Location loc = Util.StringToLocation(main.getConfig().getString("spawn.act1"));
		Player player = getPlayer();
		if (player != null) {
			player.teleport(loc);
		}
	}
}
