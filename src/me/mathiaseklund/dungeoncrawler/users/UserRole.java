package me.mathiaseklund.dungeoncrawler.users;

public enum UserRole {
	BANNED, PLAYER, STAFF
}
