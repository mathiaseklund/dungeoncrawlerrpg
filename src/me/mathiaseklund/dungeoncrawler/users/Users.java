package me.mathiaseklund.dungeoncrawler.users;

import java.util.HashMap;

import org.bukkit.entity.Player;

import me.mathiaseklund.dungeoncrawler.Main;
import me.mathiaseklund.dungeoncrawler.util.Util;

public class Users {

	// All currently online users
	static HashMap<String, User> onlineusers = new HashMap<String, User>();

	/**
	 * Create player user object
	 * 
	 * @param uuid
	 */
	public static void loginUser(String uuid) {
		Main main = Main.getMain();
		User user = new User(uuid);
		onlineusers.put(uuid, user);
		Player player = user.getPlayer();
		if (player != null) {
			String location = main.getConfig().getString("spawn.lobby");
			if (location != null) {
				player.teleport(Util.StringToLocation(location));
			}
		}
	}

	/**
	 * Remove players user object
	 * 
	 * @param uuid
	 */
	public static void logoutUser(String uuid) {
		if (onlineusers.containsKey(uuid)) {
			onlineusers.remove(uuid);
		}
	}

	/**
	 * Get players user object
	 * 
	 * @param uuid
	 * @return
	 */
	public static User getUser(String uuid) {
		if (onlineusers.containsKey(uuid)) {
			return onlineusers.get(uuid);
		} else {
			return null;
		}
	}

	/**
	 * Check if user is a staff member
	 * 
	 * @param uuid
	 * @return
	 */
	public static boolean isStaff(String uuid) {
		User user = getUser(uuid);
		UserRole role = user.getRole();
		if (role == UserRole.STAFF) {
			return true;
		} else {
			return false;
		}
	}
}
